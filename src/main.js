import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import {mapGetters} from "vuex";
import {initAxios, initAuth, initModal, initGlobalFilters} from "@/utils/init";

Vue.config.productionTip = false;
Vue.router = router;

initAxios(process.env.VUE_APP_API_URL || `https://dev.push.money/api/`);

initAuth();

initModal(router, store);

Vue.mixin({computed: mapGetters(['lang'])});

initGlobalFilters();

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');

