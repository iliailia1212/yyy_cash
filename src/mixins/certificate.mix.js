import {mapGetters, mapState} from "vuex";

export default {

    computed: {
        link_id(){
            return this.$route.params.link_id;
        },
        categories(){
            if(!this.$route.params.category) return false;
            return this.$route.params.category.split(',');
        },
        shopId(){
            return this.$route.params.transfer;
        },
        transfer(){
            const transfer = this.getTransfer({
                categories: this.categories,
                shop: this.shopId,
            });
            if(!transfer && this.spendList) this.$router.go(-1);
            return transfer;
        },
        name(){
            if(this.transfer) return this.transfer.name;
        },
        ... mapState('transfers', ['spendList']),
        ...mapGetters('transfers', ['getTransfer'])
    },
}