import {mapState} from "vuex";

export default {
    computed: {
        theme() {
            if(!this.$route.matched.some(({name})=>name==='push_template')) return;
            if (this.walletInfo && this.custom && this.custom.background_name) {
                if(this.walletInfo.customization_id !== null){
                    return 'theme-' + this.custom.background_name;
                }
            }
        },
        ...mapState('wallet', ['walletInfo']),
        ...mapState('custom', ['custom'])
    },
    watch: {
        theme(v){
            document.body.className = v;
        }
    }
}