
import Vue from 'vue';
import Vapi from "vuex-rest-api"
import {stateLocalStorage} from "@/utils/store";


const spendListTimeState = stateLocalStorage('spend-list-time');
const spendListState = stateLocalStorage('spend-list', 'spendList');
const shopsState = stateLocalStorage('shops-list', 'shops');
const categoriesState = stateLocalStorage('spend-categories', 'categories');
const shopsTopState = stateLocalStorage('spend-shops-top', 'shops_top');

let spendList = undefined;
let shops = undefined;
let categories = undefined;
let shops_top = undefined;

try {
    const spendListTime = parseInt(spendListTimeState.get());
    const time_cache = (process.env.NODE_ENV === 'development') ? 60 : 60*60*1000;
    if(!isNaN(spendListTime) && new Date().getTime()-spendListTime < time_cache){
        spendList = spendListState.get();
        categories = categoriesState.get();
        shops = shopsState.get();
        shops_top = shopsTopState.get();
    }
}
catch (e) {console.warn(e);}

const api = new Vapi({
    axios: Vue.axios,
    state: {
        /*categories: [
            {
                id: '???',
                name: 'Дом',
                color: '#AD6B06'
            },
            {
                id: '???',
                name: 'Отдых и развлечения',
                color: '#C7E686'
            },
            {
                id: '???',
                name: 'Красота и здоровье',
                color: '#E686A8'
            },
            {
                id: 'tickets',
                name: 'Кино и TV',
                color: '#8B1FF6'
            },
            {
                id: '???',
                name: 'Украшения и часы',
                color: '#F9EB9C'
            },
            {
                id: 'kids',
                name: 'Для детей',
                color: '#64E02A'
            },
            {
                id: 'books',
                name: 'Книги',
                color: '#40DAB5'
            },
            {
                id: 'cosmetics',
                name: 'Косметика',
                color: '#EA2C70'
            },
            {
                id: 'clothing',
                name: 'Одежда и обувь',
                color: '#2521D9'
            },
            {
                id: 'tech',
                name: 'Техника',
                color: '#1E65EF'
            },
            {
                id: ['food', 'grocery'],
                name: 'Еда и доставка',
                color: '#D86525'
            },

            {
                id: 'games',//TODO
                name: 'Игры',
                color: '#F61F46'
            },
            {
                id: 'communication',
                name: 'Связь',
                color: '#1FC3F6'
            },
            {
                id: '???',
                name: 'Путешествия',
                color: '#47D844'
            },
            {
                id: '???',
                name: 'Такси и каршеринг',
                color: '#EAAD11'
            },
            {
                id: '???',
                name: 'Обучение',
                color: '##8FFCE'
            },
            {
                id: 'gas',
                name: 'АЗС',
                color: '#EAAD11'
            },
        ],*/
        transfers: {
            /*communication: [
                {
                    id: 'BipToPhone',
                    title: {
                        ru: 'BipToPhone',
                        en: 'BipToPhone'
                    },
                    icon: 'phone',
                    link: 'push.send.mobile',
                    category: 'communication'
                }
            ],*/
            games: [
                {
                    id: 'Timeloop',
                    title: {
                        ru: 'Timeloop',
                        en: 'Timeloop'
                    },
                    icon: 'timeloop',
                    link: 'push.send.timeloop',
                    disabled: true,
                    category: 'games'
                }
            ]
        },
        partners: [
            {
                name: 'Giftery',
                slug: 'giftery'
            },
            {
                name: 'GIFT',
                slug: 'gift'
            },
            {
                name: 'Gratz',
                slug: '*'
            }
        ],
        categories,
        shops,
        spendList,
        shops_top
    }
})
    .get({
        action: "getSpendList",
        property: "spendList",
        path: '/spend/list',
        onSuccess(state, {data}){
            spendListState.set(state, {data: data.certificates});
            categoriesState.set(state, {data: data.categories});
            shopsState.set(state, {data: data.shops});
            shopsTopState.set(state, {data: data.shops_top});
            spendListTimeState.set(false, {data: new Date().getTime()});
        }
    })
    .getStore({namespaced: true});

export default {
    ...api,
    getters: {
        getCategory: (s) => (categories) => {
            if(!s.categories) return [];
            if(categories[0] instanceof Object) return categories[0];
            const cat = categories.find(c=>s.categories.hasOwnProperty(c));
            if(cat) return s.categories[cat];
        },
        transfersCategory: (s, g) => (categories) => {
            if(!s.spendList) return [];
            return categories.reduce((tmp, category)=>{
                const shops = Object.keys(s.spendList[category]);

                const list = shops.map((id)=>({id,category, ...s.shops[id]}));
                return [...tmp, ...list];
            }, []);
        },
        getTransfer: (s, g) => ({categories, shop}) => {
            if(!s.spendList) return;
            return s.spendList[categories][shop];
        },
        getPartner: (s) => (transfer) => {
            if(!transfer) return  {};
            return s.partners.find(({slug}) => {
                if(slug === '*') return true;
                else return transfer.products && transfer.products.some((product)=>product.slug && product.slug.includes(slug));
            }).name;
        },
        transferById: (s, g) => (id) => {
            if(!s.spendList || !s.shops.hasOwnProperty(id)) return;
            let category = Object.keys(s.spendList).find(category => s.spendList[category].hasOwnProperty(id));
            let data = {
                shop: {
                    id,
                    ...s.shops[id]
                },
                category: false
            };
            if(category) {
                data.category = {
                    id: category,
                    ...s.categories[category]
                };
            }
            return data;
        },
        transfersFind: (s, g) => (q) => {
            if(!s.spendList) return [];
            let query = q ? q.toLowerCase() : '';
            const list = s.spendList;
            let arr = [];
            let categories = Object.keys(list);
            Object.keys(s.transfers).forEach(cat => {
               if(!categories.includes(cat)) {
                   categories.push(cat);
               }
            });
            categories.map((category)=>{
                const shopIds = Object.keys(s.spendList[category]);
                let listShops = shopIds.map((id)=>({id,category, ...s.shops[id]})).filter(shop=>shop.title);

                if(s.transfers[category]) listShops.push(...s.transfers[category]);
                listShops.forEach((shop)=>{
                    if(Object.values(shop.title).join('').toLowerCase().includes(query)){
                        arr.push(shop);
                    }
                })
            });
            return arr;
        }
    }
};