import Vue from 'vue';
import Vapi from "vuex-rest-api"
import {stateLocalStorage} from "@/utils/store"

const userState = stateLocalStorage('user');

const api = new Vapi({
    axios: Vue.axios,
    state: {
        user: userState.get()
    }
})
    .post({
        action: "login",
        property: "user",
        path: '/auth/login/email',
        onSuccess: userState.set
    })
    .get({
        action: "loginGoogle",
        property: "user",
        path: '/login/google-oauth2',
        onSuccess: userState.set
    })
    .get({
        action: "loginTelegram",
        property: "user",
        path: '/login/telegram',
        onSuccess: userState.set
    })
    .getStore({namespaced: true});

export default {
    ...api,
};