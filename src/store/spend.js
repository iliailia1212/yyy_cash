import Vue from 'vue';
import Vapi from "vuex-rest-api"
import {stateLocalStorage} from "@/utils/store";

const spendState = stateLocalStorage('spend');

const api = new Vapi({
    axios: Vue.axios,
    state: {
        spend: spendState.get()
    }
})
    .post({
        action: "getSpend",
        property: "spend",
        path: ({link_id}) => `/spend/${link_id}`,
        onSuccess: spendState.set
    })
    .post({
        action: "getSpendInfo",
        property: "spendInfo",
        path: ({link_id}) => `/spend/${link_id}?confirm=0`
    })
    .getStore({namespaced: true});

export default {
    ...api
};