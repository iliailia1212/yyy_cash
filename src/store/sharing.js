import Vue from 'vue';
import Vapi from "vuex-rest-api"
import {stateLocalStorage} from "@/utils/store";

const sharingCreateState = stateLocalStorage('sharingCreateResult');

const api = new Vapi({
    axios: Vue.axios,
    state: {
        sharingCreateResult: sharingCreateState.get(),
        activeTab: 'general',
        recipientData: {
            companyName: "",
            googleLink: "",
            target: false,
            campaign_pass: "",
            wallet_pass: ""
        }
    }
})
    .post({
        action: "validateSource",
        property: "validateSourceResult",
        path: '/sharing/validate-source'
    })
    .post({
        action: "sharingCreate",
        property: "sharingCreateResult",
        path: '/sharing/create',
        onSuccess: sharingCreateState.set
    })
    .get({
        action: "getCheckPayment",
        property: "checkPayment",
        path: ({campaign_id}) => `/sharing/${campaign_id}/check-payment`
    })
    .post({
        action: "getStats",
        property: "stats",
        path: ({campaign_id}) => `/sharing/${campaign_id}/stats`
    })
    .post({
        action: "getStatsExtended",
        property: "statsExtended",
        path: ({campaign_id}) => `/sharing/${campaign_id}/stats?extended=1`
    })
    .post({
        action: "closeSharing",
        property: "closeSharingResult",
        path: ({campaign_id, confirm = false}) => `/sharing/${campaign_id}/close${confirm?'?confirm=1':''}`
    })
    .getStore({namespaced: true});

export default {
    ...api,
    getters: {
        dataCreate(s){
            let data = {
                sender: s.recipientData.companyName,
                source: s.recipientData.googleLink
            };
            if(s.recipientData.target) data.target = s.recipientData.target;
            if(s.recipientData.wallet_pass) data.wallet_pass = s.recipientData.wallet_pass;
            if(s.recipientData.campaign_pass) data.campaign_pass = s.recipientData.campaign_pass;
            return data;
        }
    },
    mutations: {
        setActiveTab(s, tab){
            s.activeTab = tab;
        },
        recipientDataSet(s, {name, val}){
            s.recipientData[name] = val;
        },
        recipientDataClean(s){
            s.recipientData.companyName = '';
            s.recipientData.googleLink = '';
            s.recipientData.target = false;
            s.recipientData.password = '';
        },
        ...api.mutations
    }
};