import Vue from 'vue';
import Vapi from "vuex-rest-api"

const api = new Vapi({
    axios: Vue.axios,
    state: {
        questionnaireData: {
            name: "",
            companyName: "",
            password: "",
            amount: "",
            wallet_pass: "",
            questions: [""],
            participants: "",
        }
    }
})

    .get({
        action: "a",
        property: "a",
        path: '/'
    })

    .getStore({namespaced: true});

export default {
    ...api,
    mutations: {
        questionnaireDataSet(s, {name, val}){
            s.questionnaireData[name] = val;
        },
        ...api.mutations
    }
};