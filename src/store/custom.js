import Vue from 'vue';
import Vapi from "vuex-rest-api"
import {stateLocalStorage} from "@/utils/store";

const customState = stateLocalStorage('custom');

const dataCustom = {
    animation_name: '',
    background_name: '',
    head_text: '',
    animation_text: '',
    logo_image_id: {id: null},
    email_image_id: {id: null},
    target_shop: {id: null},

    email_subject_text: null,
    email_head_text: null,
    email_body_text: null,
    email_button_text: null,

    activeBgLinearIndex: false,
    activeAnimationIndex: 0,
};

const api = new Vapi({
    axios: Vue.axios,
    state: {
        custom: customState.get(),
        dataCustom: Object.assign({}, dataCustom),
        isUsedCustom: false
    }
})
    .post({
        action: "createCustom",
        property: "createCustomResult",
        path:  '/custom/create-setting',
    })
    .get({
        action: "getCustom",
        property: "custom",
        path: ({setting_id}) => `/custom/get-setting/${setting_id}`,
        onSuccess: customState.set
    })
    .post({
        action: "upload",
        property: "uploadResult",
        path:  '/upload',
    })
    .get({
        action: "getUpload",
        property: "upload",
        path: ({filename}) => `/upload/${filename}`
    })
    .getStore({namespaced: true});

export default {
    ...api,
    getters: {
        customRealData(s){
            return {
                email_image_id: s.dataCustom.email_image_id.id,
                logo_image_id: s.dataCustom.logo_image_id.id,
                target_shop: s.dataCustom.target_shop.id,
                animation_name: s.dataCustom.animation_name,
                background_name: s.dataCustom.background_name,
                head_text: s.dataCustom.head_text,
                animation_text: s.dataCustom.animation_text,

                email_subject_text: s.dataCustom.email_subject_text,
                email_head_text: s.dataCustom.email_head_text,
                email_body_text: s.dataCustom.email_body_text,
                email_button_text: s.dataCustom.email_button_text
            }
        }
    },
    mutations: {
        cleanCustomResult(s){
            s.createCustomResult = null;
        },
        cleanDataCustom(s){
            Object.keys(dataCustom).forEach(key=>{
                s.dataCustom[key] = dataCustom[key];
            });
            s.isUsedCustom = false;
        },
        setIsUsedCustom(s, val){
            s.isUsedCustom = val;
        },
        setDataCustom(s, {name, val}){
            s.dataCustom[name] = val;
        },
        cleanCustom(s){
            customState.set(s, {});
        },
        ...api.mutations
    },
};