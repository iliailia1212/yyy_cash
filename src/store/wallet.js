import Vue from 'vue';
import Vapi from "vuex-rest-api"
import {stateLocalStorage} from "@/utils/store";

const pushCreateState = stateLocalStorage('pushCreateResult');

const api = new Vapi({
    axios: Vue.axios,
    state: {
        wallet: {
            sender: null,
            recipient: null,
            amount: null,
            coin: 'BIP'
        },
        pushCreateResult: pushCreateState.get()
    }
})
    .post({
        action: "pushCreate",
        property: "pushCreateResult",
        path: '/push/create',
        onSuccess: pushCreateState.set
    })
    .get({
        action: "getWalletInfo",
        property: "walletInfo",
        path: ({link_id}) => `/push/${link_id}/info`
    })
    .post({
        action: "getMnemonic",
        property: "mnemonic",
        path: ({link_id}) => `/push/${link_id}/mnemonic`
    })
    .post({
        action: "getWalletBalance",
        property: "walletBalance",
        path: ({link_id}) => `/push/${link_id}/balance`
    })
    .getStore({namespaced: true});

export default {
    ...api,
    mutations: {
        walletSet(s, {name, val}){
            s.wallet[name] = val;
        },
        walletClean(s){
            Object.keys(s.wallet).forEach(key=>{
                s.wallet[key] = null;
            });
            s.wallet.coin = 'BIP';
        },
        ...api.mutations
    }
};