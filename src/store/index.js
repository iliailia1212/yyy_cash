import Vue from 'vue'
import Vuex from 'vuex'

import wallet from './wallet'
import spend from './spend'
import sharing from './sharing'
import transfers from './transfers'
import questionnaire from './questionnaire'
import custom from './custom'
import user from './user'
import main from './main'
import campaign from './campaign'

import moment from "moment";

Vue.use(Vuex);

function langSys() {
  var language = window.navigator ? (window.navigator.language ||
      window.navigator.systemLanguage ||
      window.navigator.userLanguage) : "en";
  return  language.substr(0, 2).toLowerCase();
}
let lang = localStorage.getItem('lang') || langSys();
moment.locale(lang);

export default new Vuex.Store({
  state: {
    languagesList: require('../langs.json'),
    selectedLanguage: lang,
    modalOpened: false,
  },
  getters: {
    lang(s){
      return s.languagesList[s.selectedLanguage];
    },
    isPC(){
      return !(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(window.navigator.userAgent));
    }
  },
  mutations: {
    selectLanguage(s, lang){
      s.selectedLanguage = lang;
      localStorage.setItem('lang', lang);
      moment.locale(lang);
    },
    openModal(s){
      s.modalOpened = true;
    },
    closeModal(s){
      s.modalOpened = false;
    },
  },
  modules: {
    wallet,
    spend,
    sharing,
    transfers,
    custom,
    questionnaire,
    user,
    main,
    campaign
  }
})
