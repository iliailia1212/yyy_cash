import Vue from 'vue';
import Vapi from "vuex-rest-api"
import {stateLocalStorage} from "@/utils/store";

const campaignsState = stateLocalStorage('campaigns');

const api = new Vapi({
    axios: Vue.axios,
    state: {
        campaigns: campaignsState.get([]),
        campaign: {},
        tasksTypes: {
            youtube: {
                name: "YouTube",
                img: require("@/assets/devTmp/media-task-img-tmp.png"),
                linkPlaceholder: 'https://youtube.com/...',

                types: {
                    watch: 'Нахождение на странице',
                    subscribe: 'Подписка на канал',
                    like: 'Поставить лайк на видео'
                }
            }
        }
    }
})
    .post({
        action: "createCampaign",
        property: "createdCampaign",
        path: '/rewards/campaign'
    })
    .get({
        action: "getCampaign",
        property: "campaign",
        path: ({id}) => `/rewards/campaign/${id}`
    })
    .getStore({namespaced: true});

export default {
    ...api,
    mutations: {
        campaignsSet(s, data){
            campaignsState.set(s, {data});
        },
        campaignsAdd(s, data){
            campaignsState.add(s, {data});
        },
        campaignUpdate(s, campaignNew){
            campaignsState.update(s, (campaign)=>{
                if(campaign.id === campaignNew.id) return campaignNew;
                else return campaign;
            });
        },
        ...api.mutations
    },
};