import Vue from 'vue';
import Vapi from "vuex-rest-api"

const api = new Vapi({
    axios: Vue.axios,
    state: {
        exchangeRates: {},
        customCoins: []
    }
})
    .get({
        action: "getExchangeRates",
        property: "exchangeRates",
        path: '/exchange-rates'
    })
    .get({
        action: "getCustomCoins",
        property: "customCoins",
        path: '/custom-coins',
        onSuccess(state, {data}){
            state.customCoins = data && data.symbols || [];
        }
    })
    .post({
        action: "createWallet",
        property: "wallet",
        path: '/push/create',
    })
    .get({
        action: "getDeeplink",
        property: "deeplink",
        path({address, amount, coin, nofee}) {
            let defaultPath = `/deeplink?address=${address}&amount=${amount}&coin=${coin}`
            return nofee ? defaultPath + `&nofee` : defaultPath
        }
    })
    .getStore({namespaced: true});

export default api;