import Vue from 'vue'
import axios from "axios";
import VueAxios from "vue-axios";
import VueAuth from '@websanova/vue-auth'
import router from "@/router";
import store from "@/store";

export let initAxios = (urlApi) => {
    //https://static.255.135.203.116.clients.your-server.de/api
    //https://push.money/api/
    Vue.use(VueAxios, axios);

    Vue.axios.defaults.baseURL = urlApi;

    Vue.axios.interceptors.response.use(function (res) {
        if(!res.data || res.data.error) return Promise.reject(res.data.error);
        return res;
    }, function (e) {
        return Promise.reject(e);
    });

    axios.interceptors.request.use(function (config) {
        const pass = localStorage.getItem('password');
        if(pass){
            const key = config.method === 'get' ? 'body' : 'data';
            let data = config[key];
            if(!data) data = {};
            data['password'] = pass;
            config[key] = data;
        }
        return config;
    }, function (error) {
        return Promise.reject(error);
    });

    Vue.axios.interceptors.response.use(function (res) {
        if(!res.data || res.data.error) return Promise.reject(res.data.error || res.data.errors.join(';'));
        return res;
    }, function (e) {
        const data = e.response.data;
        if(data && (data.error || data.errors)) {
            if(data.error) return Promise.reject(data.error);
            if(data.errors) {
                const err = Object.keys(data.errors)
                    .map(key => `${key}: ${data.errors[key]}`)
                    .join('; ');
                return Promise.reject(err);
            }
        }
        return Promise.reject(e);
    });
};

export let initAuth = () => {
    Vue.use(VueAuth, {
        auth: require('@websanova/vue-auth/drivers/auth/basic.js'),
        http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
        router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
        refreshData: {
            enabled: false
        }
    });
};

export let initModal = (router, store) => {
    router.afterEach((to, from) => {
        store.commit('closeModal', false);
    });
};

export let initGlobalFilters = () => {
    Vue.filter('amount', (v) => {
        const val = parseFloat(v);
        if (val === parseInt(v)) return v;
        else return val.toFixed(2);
    });
};