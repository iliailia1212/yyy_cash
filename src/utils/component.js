export let getModel = (method, state) => (name) => ({
    get(){
        return this[state][name];
    },
    set(val){
        this[method]({name, val});
    }
});

export let getUrlHome = ($route) => {
    const name = $route.name;
    if(name && (name.includes('push.') || name.includes('certificate.') || name === 'push')){
        return {
            name: 'push',
            link_id: $route.params.link_id
        }
    }
    else return '/';
};

export let localItemsHistory = {
    get(name){
        let list = [];
        try {
            let val = localStorage.getItem(name);
            if(val) {
                val = JSON.parse(val);
                if(Array.isArray(val)) list = val;
            }
        }
        catch (e) {
            console.warn(e);
        }
        return list;
    },
    add(name, data) {
        let list = this.get(name);
        list.push({
            date: new Date().getTime(),
            ...data
        });
        localStorage.setItem(name, JSON.stringify(list));
    }
};