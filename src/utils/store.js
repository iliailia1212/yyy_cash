export let stateLocalStorage = (name, state = false) => ({
    get(defaultVal = null){
        let state = defaultVal;
        try {
            if(localStorage.hasOwnProperty(name)){
                state = JSON.parse(localStorage.getItem(name));
            }
        }
        catch (e) {console.warn(e, localStorage.getItem(name));}
        return state;
    },
    set(s, {data}){
        if(data === undefined) localStorage.removeItem(name);
        else localStorage.setItem(name, JSON.stringify(data));
        if(s) s[state?state:name] = data;
    },
    update(s, handler){
        let list = this.get([]);
        list = list.map((data)=>handler(data));
        this.set(s, {data: list})
    },
    add(s, {data}){
        let list = this.get([]);
        list.push(data);
        this.set(s, {data: list})
    }
});