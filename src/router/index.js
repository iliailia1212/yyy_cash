import Vue from 'vue'
import VueRouter from 'vue-router'
import MyPushes from '../views/MyPushes.vue'
import MyCampaigns from '../views/MyCampaigns.vue'
import Index from '../views/Index.vue'
import About from '../views/About.vue'
import AboutMerchant from '../views/AboutMerchant.vue'

import NewPush from '../views/newPush/NewPush.vue'
import Password from '../views/newPush/Password.vue'
import walletPreview from '../views/newPush/walletPreview.vue'
import Paying from '../views/newPush/Paying.vue'
import Share from '../views/newPush/Share.vue'

import _templateOwned from '../views/owned/_templateOwned.vue'
import Owned from '../views/owned/Owned.vue'
import OwnedPassword from '../views/owned/OwnedPassword.vue'
import Search from '../views/owned/Search.vue'
import Phrase from '../views/owned/Phrase.vue'
import topUp from '../views/owned/topUp.vue'
import Сategory from '../views/owned/Сategory.vue'
import Send from '../views/owned/Send.vue'
import Sent from '../views/owned/Sent.vue'

import SendShop from '../views/shop/SendShop.vue'

import Recipients from '../views/recipients/Recipients.vue'
import RecipientsPaying from '../views/recipients/RecipientsPaying.vue'
import RecipientsShare from '../views/recipients/RecipientsShare.vue'
import RecipientsExample from '../views/recipients/Example.vue'

import _templateCertificate from '../views/certificate/_templateCertificate.vue'
import Certificate from '../views/certificate/Certificate.vue'
import HowToUseCertificate from '../views/certificate/HowToUse.vue'
import MyCertificate from '../views/certificate/MyCertificate.vue'

import CampaignStatsPassword from '../views/campaignStats/PasswordStats.vue'
import CampaignStatsStats from '../views/campaignStats/Stats.vue'
import CampaignStatsStop from '../views/campaignStats/Stop.vue'

import CustomizationOffer from '../views/newPush/сustomization/CustomizationOffer.vue'
import CustomizationSetting from '../views/newPush/сustomization/CustomizationSetting.vue'

import Login from '../views/signIn/Login.vue'
import Registration from '../views/signIn/Registration.vue'
import ForgotPassword from '../views/signIn/ForgotPassword.vue'
import ChangePassword from '../views/signIn/ChangePassword.vue'

import DashboardCampaigns from '../views/campaigns/DashboardCampaigns.vue'
import CreateTaskMedia from '../views/campaigns/CreateTaskMedia.vue'
import CreateTaskGoal from '../views/campaigns/CreateTaskGoal.vue'
import CreateTaskForm from '../views/campaigns/CreateTaskForm.vue'
import CampaignStats from '../views/campaigns/Stats.vue'

import _templateAccount from '../views/account/_templateAccount.vue'
import Profile from '../views/account/Profile.vue'
import Dashboard from '../views/account/Dashboard.vue'
import Withdrawal from '../views/account/Withdrawal.vue'
import AddBrand from '../views/account/AddBrand.vue'
import EditBrand from '../views/account/EditBrand.vue'
import AddShop from '../views/account/AddShop.vue'
import IntegratedShop from '../views/account/IntegratedShop.vue'
import AddGoods from '../views/account/AddGoods.vue'
import EditShop from '../views/account/EditShop.vue'
import Goods from '../views/account/Goods.vue'

const EmptyTemplate = {render: (r) => r('router-view')};

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Index',
    component: Index,
    meta: {
      contentClass: 'mainFlex'
    }
  },
  {
    path: '/about',
    name: 'about',
    component: About,
  },
  {
    path: '/about-merchant',
    name: 'aboutMerchant',
    component: AboutMerchant,
  },
  {
    path: '/pushes',
    name: 'pushes',
    component: MyPushes
  },
  {
    path: '/sharings',
    name: 'sharings',
    component: MyCampaigns
  },
  {
    path: '/pushes/new',
    name: 'newPush',
    component: NewPush
  },
  {
    path: '/pushes/new/step2',
    name: 'newPush.step2',
    component: Password
  },
  {
    path: '/pushes/new/customization-offer',
    name: 'newPush.customizationOffer',
    component: CustomizationOffer
  },
  {
    path: '/pushes/new/customization-setting',
    name: 'newPush.customizationSetting',
    component: CustomizationSetting
  },
  {
    path: '/pushes/new/walletPreview',
    name: 'newPush.preview',
    component: walletPreview
  },
  {
    path: '/pushes/created/:link_id',
    name: 'newPush.created',
    component: Paying,
    meta: {
      contentClass: 'static'
    }
  },
  {
    path: '/pushes/share/:link_id',
    name: 'newPush.share',
    component: Share
  },
  {
    path: '/push/:link_id/',
    component: _templateOwned,
    //name: 'push_template',
    children: [
      {
        path: '',
        name: 'push',
        component: Owned
      },
      {
        path: 'password',
        name: 'push.password',
        component: OwnedPassword
      },
      {
        path: 'search/:search?',
        name: 'push.search',
        component: Search
      },
      {
        path: 'phrase',
        name: 'push.phrase',
        component: Phrase
      },
      {
        path: 'topUp',
        name: 'push.topUp',
        component: topUp
      },
      {
        path: 'category/:category',
        name: 'push.category',
        component: Сategory,
        meta: {
          readonly: true
        }
      },
      {
        path: 'transfer',
        name: 'push.send.transfer',
        component: Send,
        meta: {
          category: 'transfer',
          sendTo: 'wallet',
          amountLimit: false,
          lastWallet: true
        }
      },
      {
        path: 'resend',
        name: 'push.send.resend',
        component: NewPush,
        meta: {
          reSend: true
        }
      },
      {
        path: 'resend/password',
        name: 'push.send.resend.password',
        component: Password,
        meta: {
          reSend: true
        }
      },
      {
        path: 'resend/share',
        name: 'push.send.resend.share',
        component: Share,
        meta: {
          reSend: true
        }
      },
      {
        path: 'timeloop',
        name: 'push.send.timeloop',
        component: Send,
        meta: {
          category: 'timeloop',
          amountLimit: true,
        }
      },
      {
        path: 'mobile',
        name: 'push.send.mobile',
        component: Send,
        meta: {
          category: 'mobile',
          amountLimit: true
        }
      },
      {
        path: 'successfully',
        name: 'push.successfully',
        component: Sent
      },

      {
        path: ':category?/:transfer/certificate/',
        component: _templateCertificate,
        children: [
          {
            path: '',
            name: 'push.send.certificate',
            component: Certificate
          },
          {
            path: 'how-to-use',
            name: 'certificate.how-to-use',
            component: HowToUseCertificate
          },
          {
            path: 'successfully',
            name: 'certificate.successfully',
            component: MyCertificate
          },
        ]
      },
      {
        path: ':category?/:transfer/shop/',
        component: _templateCertificate,
        children: [
          {
            path: '',
            name: 'push.send.shop',
            component: SendShop,
            meta: {
              category: 'transfer',
              sendTo: 'wallet',
              amountLimit: false,
              lastWallet: true
            }
          },
          {
            path: 'successfully',
            name: 'shop.successfully',
            component: MyCertificate//TODO
          },
        ]
      },
    ]
  },

/*  {
    path: '/questionnaire',
    name: 'questionnaire',
    component: Questionnaire
  },
  {
    path: '/questionnaire/example',
    name: 'questionnaire.example',
    component: QuestionnaireExample
  },
  {
    path: '/questionnaire/:questionnaire_id/playing',
    name: 'questionnaire.playing',
    component: QuestionnairePaying
  },
  {
    path: '/questionnaire/:questionnaire_id/successfully',
    name: 'questionnaire.successfully',
    component: QuestionnaireShare
  },
  {
    path: '/questionnaire/:questionnaire_id',
    name: 'questionnaire.pass',
    component: passQuestionnaire
  },*/

  {
    path: '/recipients',
    name: 'recipients',
    component: Recipients
  },
  {
    path: '/recipients/customization-offer',
    name: 'recipients.customizationOffer',
    component: CustomizationOffer,
    meta: {
      recipients: true
    }
  },
  {
    path: '/recipients/customization-setting',
    name: 'recipients.customizationSetting',
    component: CustomizationSetting,
    meta: {
      recipients: true
    }
  },
  {
    path: '/recipients/walletPreview',
    name: 'recipients.preview',
    component: walletPreview
  },
  {
    path: '/recipients/:campaign_id/created/',
    name: 'recipients.created',
    component: RecipientsPaying,
    meta: {
      contentClass: 'static'
    }
  },
  {
    path: '/recipients/:campaign_id/successfully/',
    name: 'recipients.successfully',
    component: RecipientsShare,
    meta: {
      contentClass: 'static'
    }
  },
  {
    path: '/recipients/example',
    name: 'recipients.example',
    component: RecipientsExample
  },

/*    /!* Q STATS *!/
  {
    path: '/stats/:questionnaire_id/password',
    name: 'qStats.password',
    component: QuestionnaireStatsPassword
  },
  {
    path: '/stats/:questionnaire_id/general',
    name: 'qStats.general',
    component: QuestionnaireStatsGeneral
  },
  {
    path: '/stats/:questionnaire_id/personal/',
    name: 'qStats.personal',
    component: QuestionnaireStatsPersonal
  },
  {
    path: '/qStats/:questionnaire_id/stop/',
    name: 'qStats.stop',
    component: QuestionnaireStatsStop
  },*/
  {
    path: '/qStats/:questionnaire_id/successfully/',
    name: 'qStats.successfully',
    component: Sent,
    meta: {
      backLink: 'about'
    }
  },
  {
    name: 'qStats.short',
    path: '/qStats/:campaign_id',
    redirect: {name: 'qStats.general'}
  },
    /* STATS */
  {
    path: '/stats/:campaign_id/password',
    name: 'campaignStats.password',
    component: CampaignStatsPassword
  },
  {
    path: '/stats/:campaign_id',
    name: 'campaignStats.stats',
    component: CampaignStatsStats
  },
  {
    path: '/stats/:campaign_id/stop/',
    name: 'campaignStats.stop',
    component: CampaignStatsStop
  },
  {
    path: '/stats/:campaign_id/successfully/',
    name: 'campaignStats.successfully',
    component: Sent,
    meta: {
      backLink: 'about'
    }
  },
  {
    name: 'campaignStats.short',
    path: '/stats/:campaign_id',
    redirect: {name: 'campaignStats.general'}
  },
  {
    name: 'login',
    path: '/login',
    component: Login,
  },
  {
    name: 'registration',
    path: '/registration',
    component: Registration,
  },
  {
    name: 'forgotPassword',
    path: '/forgot-password',
    component: ForgotPassword,
  },
  {
    name: 'changePassword',
    path: '/change-password',
    component: ChangePassword,
  },
  {
    path: '/account',
    component: _templateAccount,
    children: [
      {
        name: 'profile',
        path: '/profile',
        component: Profile,
      },
      {
        name: 'dashboard',
        path: 'dashboard',
        component: Dashboard,
      },
    ]
  },
  {
    name: 'withdrawal',
    path: '/withdrawal',
    component: Withdrawal,
  },
  {
    name: 'addBrand',
    path: '/add-brand',
    component: AddBrand,
  },
  {
    name: 'editBrand',
    path: '/edit-brand',
    component: EditBrand,
  },
  {
    name: 'addShop',
    path: '/add-shop',
    component: AddShop,
  },
  {
    name: 'integratedShop',
    path: '/integrated-shop',
    component: IntegratedShop,
  },
  {
    name: 'addGoods',
    path: '/add-goods',
    component: AddGoods,
  },
  {
    name: 'editShop',
    path: '/edit-shop',
    component: EditShop,
  },
  {
    name: 'goods',
    path: '/goods',
    component: Goods,
  },

  {
    path: '/campaigns',
    component: EmptyTemplate,
    children: [
      {
        name: 'campaigns.dashboard',
        path: 'dashboard',
        component: DashboardCampaigns,
      },
      {
        name: 'campaigns.create',
        path: 'create',
        component: CreateTaskMedia,
      },
      {
        name: 'campaigns.create.step2',
        path: 'create/:media',
        component: CreateTaskGoal,
      },
      {
        name: 'campaigns.create.step3',
        path: 'create/:media/:goal',
        component: CreateTaskForm,
      },
      {
        name: 'campaigns.successfully',
        path: 'successfully',
        component: Sent,
        meta: {
          backLink: 'campaigns.dashboard'
        }
      },
      {
        name: 'campaigns.stats',
        path: 'stats/:id',
        component: CampaignStats,
      },
    ]
  },

  {
    path: "*",
    redirect: '/'
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
