module.exports = {
    devServer: {
        clientLogLevel: 'warn'
    },
    css: {
        loaderOptions: {
            scss: {
                prependData: `@import "~@/variables.scss";`
            },
        }
    }
};